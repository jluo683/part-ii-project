function [ output_args ] = evaluate

L = 10; % size of the deployment square(1)

lambda_ =[0.3 0.4 0.6 0.9 1.2 1.43 1.6]; % intensity of the Poisson point process, will be a changing parameter,
he = L+2; % height of the square for the triangular grid of base stations
w = L+2; % width of the square for the triangular grid of base stations
r_ = [0.5 1 2 3 4];
% radius of the hexagonal cell
MANET_R_ = [0.5 1 2]; % MANET broadcast communication radius
time_threashold_seconds = 0.01; % limit the time Charikar will spend due to i, if exceed the stated time, Charikar will not run for the rest of i but shown as NaN
%lambda, p, Radius, varargin, he, w, r, R,Terminals,i
pH_ = [0 0.1 0.25 0.3 0.5];
gamma_ = [0.1 0.25 0.5 0.75 0.9];
approximate_no_of_terminals = 1/2*lambda_(length(lambda_))^lambda_(length(lambda_));
no_of_deployment = 50;
no_of_loops = length(lambda_)*length(pH_)*length(gamma_)*length(r_)*length(MANET_R_)*no_of_deployment*approximate_no_of_terminals*9;% 9 is the upper bound of i
Klein_Ravi_SteinerTreeCost = zeros(1,no_of_loops);
Charikar_SteinerTreeCost = zeros(1,no_of_loops);
Klein_Ravi_time = zeros(1,no_of_loops);
Charikar_time = zeros(1,no_of_loops);
no_Klein_Ravi_SteinerNode = zeros(1,no_of_loops);
no_Charikar_SteinerNode = zeros(1,no_of_loops);
lambda = zeros(1,no_of_loops);
pr = zeros(1,no_of_loops);
r = zeros(1,no_of_loops);
MANET_R = zeros(1,no_of_loops);
i = zeros(1,no_of_loops);
gamma = zeros(1,no_of_loops);
number_of_terminals = zeros(1,no_of_loops);
index = 0;
for k = 1:length(lambda_)
    for l = 1: length(pH_)
        for m = 1:length(gamma_)
            pC = (1-pH_(l))/(1+gamma_(m));
            p = [pC pC*gamma_(m) pH_(l)];
            for n = 1: length(r_)
                for R = 1: length(MANET_R_)
                    for deployment_no = 1 : no_of_deployment
                        [points points_type] = generateDeployment(L, lambda_(k), p, MANET_R_);
                        
                        [nNodes dummy] = size(points);
                        
                        %[ adjacencymatrix_k nodeCosts_k ] = graph_transformation( points, points_type,he,w,r,MANET_R);
                        [adjacencymatrix_k nodeCosts_k] = graph_transformation_without_dummy_nodes( points, points_type,he,w,r_(n),MANET_R(R));
                        
                        
                        [ci sizes] = components(adjacencymatrix_k);
                        [max_elements IND] = max(sizes);
                        number_of_terminals_ =  serdiff(union(2:min(0,max_elements),round([0.1*max_elements 0.25*max_elements 0.5*max_elements])),[0 1])
                        
                        for N = 1:length(number_of_terminals_)
                            Overtime = false;
                            for i_ = 1: floor(log2(number_of_terminals_(N))) + 1 
                                f = find(ci(1:nNodes) == IND );
                                
                                sample_of_random_indices = 0;
                                
                                while length(unique(sample_of_random_indices))~=number_of_terminals_(N)
                                    
                                    sample_of_random_indices = randi(length(f),1,number_of_terminals_(N));
                                    
                                end
                                
                                Terminals = f(sample_of_random_indices);
                                
                                
                                adjacencymatrix_c  = graph_transformation_modified_Charika(points, points_type,he,w,r_(n),MANET_R(R));
                                
                                fprintf('both graph transformation completed\n');
                                
                                tic
                                [SteinerTree, SteinerNodes, Klein_Ravi_SteinertreeCost] = Klein_Ravi(adjacencymatrix_k, nodeCosts_k, Terminals);
                                Klein_Ravi_SteinerTreeCost(index) = Klein_Ravi_SteinertreeCost;
                                Klein_Ravi_Time = toc;
                                Klein_Ravi_time(index) = Klein_Ravi_Time;
                                
                                if ~Overtime
                                    Charikar_start_time = tic
                                    [approximationTree_c, SteinerNodes_c, Charikar_SteinertreeCost] = dirSteinerTree(adjacencymatrix_c, setdiff(Terminals,Terminals(1)), Terminals(1), i_, time_threashold_seconds);
                                    Charikar_SteinerTreeCost(index) =  Charikar_SteinertreeCost;
                                    if isnan(approximationTree_c)
                                        Overtime = true;
                                    end
                                    Charikar_Time = toc(Charikar_start_time);
                                    Charikar_time(index) =  Charikar_Time;
                                else
                                    Charikar_SteinerTreeCost(index) = NaN;
                                    Charikar_time(index) = NaN;
                                end
                                
                                lambda(index) = lambda_(k);
                                pr(index) = p;
                                gamma(index) = gamma_(m);
                                r(index) = r_(n);
                                no_Klein_Ravi_SteinerNode(index) =  length(SteinerNodes);
                                no_Charikar_SteinerNode(index) =  length(SteinerNodes_c);
                                MANET_R(index) = MANET_R_(R);
                                i(index) =  i_;
                                number_of_terminals(index) =  number_of_terminals_(N);
                                index = index+1;
                            end
                        end
                    end
                    
                end
            end
        end
    end
    
    
    no_of_actual_loops = index ;
    
    Test_result = table(Klein_Ravi_SteinerTreeCost(1:no_of_actual_loops), ...
                        Charikar_SteinerTreeCost(1:no_of_actual_loops), ...
                        Klein_Ravi_time(1:no_of_actual_loops), ...
                        Charikar_time(1:no_of_actual_loops), ...
                        no_Klein_Ravi_SteinerNode(1:no_of_actual_loops), ...
                        no_Charikar_SteinerNode(1:no_of_actual_loops), ...
                        lambda(1:no_of_actual_loops), ...
                        pr(1:no_of_actual_loops), ...
                        gamma(1:no_of_actual_loops),...
                        r(1:no_of_actual_loops), ...
                        MANET_R(1:no_of_actual_loops), ...
                        i(1:no_of_actual_loops), ...
                        number_of_terminals(1:no_of_actual_loops));
                    
    writetable(Test_result,'/Users/luo/repos/project/preparation/Test_result.csv');
end




