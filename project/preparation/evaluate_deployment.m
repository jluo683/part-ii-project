function output_table = evaluate_deployment
%
% EXAMPLE
% clear all; clc; output_table = evaluate_deployment

L = 10;
he = L+2;
w = L+2;


lambda_SET = [0.9 1.43];%lambda = 1.43; % intensity of the Poisson point process

r = 1;% radius of the hexagonal cell
MANET_R = 1; % radius of MANET connectivity

time_threshold_in_seconds = 10^36; % for testing purposes take small values (e.g. 20 sec), then perhaps increase, depending on how much time it take to Charikar's algorithm to complete

pC = 0.5;
pM = 0.25;
pH = 0.25;
p = [pC pM pH];

number_of_deployments_to_generate = 1;

NUM_TERMINALS =  15;
%approximate_no_of_terminals = round(length(number_of_terminals_)*number_of_deployments_to_generate*(floor(log2(length(number_of_terminals_))) + 1));


no_of_loops = round(length(lambda_SET)*NUM_TERMINALS * ...
              number_of_deployments_to_generate * ...
              (floor(log2(NUM_TERMINALS)) + 1));
fprintf('Initially the size is %d\n', no_of_loops);

size_of_largest_connected_component = zeros(1,no_of_loops);
no_of_C = zeros(1,no_of_loops);
no_of_M = zeros(1,no_of_loops);
no_of_H = zeros(1,no_of_loops);
no_of_terminals = zeros(1,no_of_loops);
lambda_output = zeros(1,no_of_loops);


Klein_Ravi_SteinerTreeCost = zeros(1,no_of_loops);
Klein_Ravi_time = zeros(1,no_of_loops);
Klein_Ravi_pure_time = zeros(1,no_of_loops);
no_of_Klein_Ravi_SteinerNodes = zeros(1,no_of_loops);


% preKlein_Ravi_SteinerTreeCost = zeros(1,no_of_loops);
% preKlein_Ravi_time = zeros(1,no_of_loops);
% preKlein_Ravi_pure_time = zeros(1,no_of_loops);
% no_of_preKlein_Ravi_SteinerNodes = zeros(1,no_of_loops);


Charikar_SteinerTreeCost = zeros(1,no_of_loops);
Charikar_time = zeros(1,no_of_loops);
Charikar_pure_time = zeros(1,no_of_loops);
no_of_Charikar_SteinerNodes = zeros(1,no_of_loops);
no_of_Charikar_SteinerNodes_minus_dummyNodes = zeros(1,no_of_loops);

i = ones(0,no_of_loops);

index = 1;
percentage_of_work_done_iterator = 1;

for lambda = lambda_SET
    for no_of_deployment = 1 : number_of_deployments_to_generate
        [points points_type] = generateDeployment(L, lambda, p, MANET_R,false);
        [nNodes dummy] = size(points);
        [adjacencymatrix_k nodeCosts_k] = graph_transformation_without_dummy_nodes( points, points_type,he,w,r,MANET_R);
        [ci sizes] = components(adjacencymatrix_k);
        [max_elements IND] = max(sizes);
        
        %number_of_terminals_ =  setdiff(union(2:min(10,max_elements),round([0.1*max_elements 0.25*max_elements 0.5*max_elements])),[0 1]);
        
        f = find(ci(1:nNodes) == IND); % f contains indices of the nodes in the largest connected component        
        
        number_of_terminals_ =  setdiff(round([0.01:0.01:0.1 0.25 0.35 0.5 0.75 0.9]*length(f)),[0 1]);
        
        [adjacencymatrix_c first_dummy_node_index_in_Charikar] = graph_transformation_modified_Charika( points, points_type,he,w,r,MANET_R);
        
        for N = 1:length(number_of_terminals_)
                        
            
            % choosing terminals randomly
            sample_of_random_indices = randsample(length(f),number_of_terminals_(N));
%             fprintf('number_of_terminals_(N)=%d\n',number_of_terminals_(N));
%             fprintf('length(unique(sample_of_random_indices))=%d\n',length(unique(sample_of_random_indices)));
%             fprintf('length(f)=%d\n',length(f));
%             fprintf('---------------\n');
            Terminals = f(sample_of_random_indices);
            
            tic
            [SteinerTree SteinerNodes Klein_Ravi_SteinertreeCost KleinRavi_time_taken_without_allShortestPaths] = Klein_Ravi(adjacencymatrix_k, nodeCosts_k, Terminals);
            Klein_Ravi_Time = toc;            
            Klein_Ravi_SteinerTreeCost(index) = Klein_Ravi_SteinertreeCost;
            Klein_Ravi_time(index) = Klein_Ravi_Time;
            Klein_Ravi_pure_time(index) = KleinRavi_time_taken_without_allShortestPaths;
            no_of_Klein_Ravi_SteinerNodes(index) = length(SteinerNodes);

%             tic            
%             [preSteinerTree preSteinerTreeCost preSteinerTreeNodes preKleinRavi_time_taken_without_allShortestPaths] = preKleinRavi(A, C, Terminals)
%             preKlein_Ravi_Time = toc; 
%             preKlein_Ravi_SteinerTreeCost(index) = preKlein_Ravi_SteinertreeCost;
%             preKlein_Ravi_time(index) = preKlein_Ravi_Time;
%             preKlein_Ravi_pure_time(index) = preKleinRavi_time_taken_without_allShortestPaths;
%             no_of_preKlein_Ravi_SteinerNodes(index) = length(preSteinerTreeNodes);
            
            
            Overtime = false;
            for i_ = 3
          % for i_ = 1: floor(log2(number_of_terminals_(N))) + 1
                
                % recording lambda used
                lambda_output(index) = lambda;
                
                
                no_of_C(index) = length(find(strcmp(points_type(f),'C')));
                no_of_M(index) = length(find(strcmp(points_type(f),'M')));
                no_of_H(index) = length(find(strcmp(points_type(f),'H')));
                no_of_terminals(index) = number_of_terminals_(N);
                
                size_of_largest_connected_component(index) = max_elements;
                
                if i_> 1
                    Klein_Ravi_SteinerTreeCost(index) = NaN; % Klein_Ravi_SteinerTreeCost(index-1);
                    Klein_Ravi_time(index) = NaN; % Klein_Ravi_time(index-1);
                    no_of_Klein_Ravi_SteinerNodes(index) = NaN; % no_of_Klein_Ravi_SteinerNodes(index-1);
                end
                
                if ~Overtime % if there has been no overtime observed yet, enter this block
                    Charikar_start_time = tic;
                    [approximationTree_c SteinerNodes_c Charikar_SteinertreeCost Charikar_time_taken_without_allShortestPaths] = dirSteinerTree(adjacencymatrix_c, setdiff(Terminals,Terminals(1)), Terminals(1), i_, time_threshold_in_seconds);
                    Charikar_Time = toc(Charikar_start_time);
                    
                    if isempty(approximationTree_c)
                        Overtime = true; % flag up an overtime event
                        Charikar_SteinerTreeCost(index) = NaN;
                        Charikar_time(index) = NaN;
                        Charikar_pure_time(index) = NaN;
                        no_of_Charikar_SteinerNodes(index) = NaN;
                    else
                        Charikar_SteinerTreeCost(index) =  Charikar_SteinertreeCost;
                        Charikar_time(index) =  Charikar_Time;
                        Charikar_pure_time(index) = Charikar_time_taken_without_allShortestPaths;
                        no_of_Charikar_SteinerNodes(index) = length(SteinerNodes_c);
                    end
                    
                else % if there has been an overtime observed yet, enter this block
                    Charikar_SteinerTreeCost(index) = NaN;
                    Charikar_time(index) = NaN;
                    Charikar_pure_time(index) = NaN;
                    no_of_Charikar_SteinerNodes(index) = NaN;
                end
                
                number_of_dummy_nodes_in_Charikars = length(find(SteinerNodes_c>=first_dummy_node_index_in_Charikar));
                %fprintf('There are %d dummy nodes in Charikars solutions',length(find(SteinerNodes_c>=first_dummy_node_index_in_Charikar)));
                    no_of_Charikar_SteinerNodes_minus_dummyNodes(index) = no_of_Charikar_SteinerNodes(index) - number_of_dummy_nodes_in_Charikars;
                
                i(index) = i_;
                index = index+1;
            end
            
            
            percentage_of_work_done = 100*percentage_of_work_done_iterator/(length(lambda_SET)*number_of_deployments_to_generate*length(number_of_terminals_));            
            percentage_of_work_done_iterator = percentage_of_work_done_iterator + 1;
            
            if mod(percentage_of_work_done_iterator,50) == 0
                try
                    send_email_notification(['MATLAB notification: ' num2str(percentage_of_work_done) '% done...'], 'Subj');
                catch ME
                end
            end
            
            
        end
    end
end
        no_of_actual_loops = index -1;

 fprintf('Initially the size is %d\n', no_of_loops);
 fprintf('The actual size is %d\n', no_of_actual_loops);
    
proportion_of_terminals = no_of_terminals(1,1:no_of_actual_loops)./...
    (no_of_C(1,1:no_of_actual_loops) + no_of_M(1,1:no_of_actual_loops) + no_of_H(1,1:no_of_actual_loops));

proportion_of_terminals_as_percentage = round(proportion_of_terminals*10^2);


    output_table = [lambda_output(1,1:no_of_actual_loops)', ...         % 1
    size_of_largest_connected_component(1:no_of_actual_loops)',...      % 2
    no_of_C(1,1:no_of_actual_loops)', ...                               % 3
    no_of_M(1,1:no_of_actual_loops)', ...                               % 4
    no_of_H(1,1:no_of_actual_loops)', ...                               % 5
    no_of_terminals(1,1:no_of_actual_loops)', ...                       % 6
    proportion_of_terminals_as_percentage',...                          % 7    
    i(1,1:no_of_actual_loops)', ...                                     % 8
    Klein_Ravi_SteinerTreeCost(1,1:no_of_actual_loops)',...             % 9
    Klein_Ravi_time(1,1:no_of_actual_loops)',...                        % 10
    Klein_Ravi_pure_time(1,1:no_of_actual_loops)',...                   % 11
    no_of_Klein_Ravi_SteinerNodes(1,1:no_of_actual_loops)',...          % 12
    Charikar_SteinerTreeCost(1,1:no_of_actual_loops)',...               % 13
    Charikar_time(1,1:no_of_actual_loops)', ...                         % 14
    Charikar_pure_time(1,1:no_of_actual_loops)', ...                    % 15
    no_of_Charikar_SteinerNodes(1,1:no_of_actual_loops)',...            % 16
    no_of_Charikar_SteinerNodes_minus_dummyNodes(1,1:no_of_actual_loops)'];                % 17

        
%         Test_result = table(no_of_C(1:no_of_actual_loops)', ...
%             no_of_M(1:no_of_actual_loops)', ...
%             no_of_H(1:no_of_actual_loops)', ...
%             size_of_largest_connected_component(1:no_of_actual_loops)',...
%             no_of_terminals(1:no_of_actual_loops)', ...
%             i(1:no_of_actual_loops)', ...
%             Klein_Ravi_SteinerTreeCost(1:no_of_actual_loops)',...
%             Klein_Ravi_time(1:no_of_actual_loops)',...
%             no_of_Klein_Ravi_SteinerNodes(1:no_of_actual_loops)',...
%             Charikar_SteinerTreeCost(1:no_of_actual_loops)',...
%             Charikar_time(1:no_of_actual_loops)', ...
%             no_of_Charikar_SteinerNodes(1:no_of_actual_loops)');
%         
%         writetable(Test_result,'/Users/luo/repos/project/preparation/Test_result.csv');

try 
    send_email_notification(['MATLAB notification: ' 'all done!'], 'Subj');
catch ME
    
end

try
    save(['output_table-' datestr(now) '.mat'], 'output_table');
catch ME
end

end



