function [ path ] = find_shortest_path( x,y,P )
%Find_Shortset_Path
% find the shortest path from x to y where p is all pair shortest path
% successor matrix returned by e.g.matlab_BGL all_shortest_path
% Examlpe
%load graphs/clr-26-1.mat
%all_shortest_paths(A)
%all_shortest_paths(A,struct('algname','johnson'))
[n m] = size(P);
path = zeros(1,n);
path(1) = y;
last_node= y;
k = 1;
while(last_node ~= x)
    k = k +1;
    last_node =P(x,last_node);
    path(k) = last_node;
end
path = fliplr( path(1:k));
end

