function [approximationTree SteinerNodes approximationTreeCost time_taken_without_allShortestPaths] = dirSteinerTree(Costs, Terminals, root, i, time_threshold_seconds)
%
% function approximationTree = dirSteinerTree2(Costs, Terminals, root)
%
% This function implements Charikar et al's approximation algorithm for
% directed Steiner problem.
%
% INPUT:
%       
% Costs - an adjacency matrix with costs (positive numbers).
% ATTENTION: zero elements in Costs mean the absence of edges. If there are
% existing edges with zero costs, that can be made possible by using a very
% small epsilon instead of zero. I intend to fix this issue with existing
% zero cost edges later.
%
% Terminals - a set of terminals
%
% root - the root node
%
% OUTPUT 
%
% approximationTree - an adjacency matrix (with costs) of the approximation tree
%
% approximationTreeCost - the cost of the approximation tree 
% (this cost is a total cost of edges in the approximation tree) 
%
% COMMENTS
%
% This function makes use of shortest_paths.m which is a part of Matlab BGL
% library (http://dgleich.github.io/matlab-bgl/)
%
% EXAMPLE
%
% load Costs
% bg = biograph(Costs)
% view(bg);
% root = 55;
% Terminals = [1,3,6,25];
% k = length(Terminals)
% i = floor(log2(k))+1;
% [approximationTree SteinerNodes approximationTreeCost] = dirSteinerTree(Costs, Terminals, root, i, 1)
% bgApproximationTree = biograph(approximationTree);
% view(bgApproximationTree)

global C
global D % distance matrix for transitive closure
global P % pseudo-predecessor matrix for transitive closure
global V
global allTerminals
global start_time
global threshold_time
%global recursion_level


threshold_time = time_threshold_seconds;

allTerminals = Terminals;

[n m] = size(Costs);

if n~=m
    error('Check the dimension of the adjacency/cost matrix - it should be square');
end

V = 1:n; 
C = sparse(Costs);


fprintf('Obtaining the transitive closure of (V,C)\n');

[D P] = all_shortest_paths(C,struct('algname','floyd_warshall'));

if ~isempty(find(D(root, Terminals)==Inf, 1))
    fprintf('At least one of the terminals cannot be reached!\n');
    approximationTree = [];
    approximationTreeCost = Inf;
    return
end

fprintf('The transitive closure of (V,C) has been obtained!\n');

% doing recursion next

fprintf('Doing the recursion now - wait...\n');

number_of_terminals = length(Terminals);


i_initial = min(floor(log2(number_of_terminals))+1,i);
start_time = tic;
% fprintf('start_time is %f\n',start_time);
%recursion_level = 1;
approximationTree = Ai(i_initial, number_of_terminals, root, Terminals);
time_taken_without_allShortestPaths = toc(start_time);

if isnan(approximationTree)
approximationTree = [];
approximationTreeCost = NaN;
SteinerNodes = NaN;
return;
end

nzIND = find(approximationTree~=0);

approximationTree(nzIND) = C(nzIND);

approximationTreeCost = sum(sum(approximationTree)) + 0;


function T = Ai(i,k,r,X)
    %fprintf('i = %d, k = %d, root = %d\n',i,k,r);
    %X

    %[d pred] = shortest_paths(C,r); % distance from r to any other vertex
    %in the graph
    time_now = toc(start_time);
    if time_now  > threshold_time
        T = NaN;
%         fprintf('Leaving this level of recursion at time %f\n', time_now);
%         fprintf('recursion level = %d\n', recursion_level);
        return
    end
    
    if toc(start_time)  < threshold_time
%         fprintf('Entering recursion...\n');        
%         fprintf('recursion level = %d\n', recursion_level);
    end
    
    f = find(D(r,X) ~= Inf & D(r,X) ~= 0);
    
    
    if length(f) < k        
        T = sparse(zeros(length(V)));
        %fprintf('returning empty tree\n');
        return
    end
    
    if i==1 % find k terminals closest to r and output the shortest paths tree rooted at r

        [vals idx] = sort(D(r,X(f)),'ascend');
        kTerminals_found = X(f(idx(1:k)));
        T = sparse(zeros(length(V)));
        
        for w = 1:length(kTerminals_found)
            v = kTerminals_found(w);
            path = buildShortestPathfromAll_shortest_paths_BGL(r,v,P); 
            coords = [path(1:end-1)' path(2:end)'];
            for z = 1:length(path)-1
                T(coords(z,1),coords(z,2)) = 1; % forget the costs - T will just accumulate non-zero elements which will show edges' presence; will sort out costs in the initial topology
            end
        end
        %fprintf('returning something and leaving this recursion level\n');
        return
    end
    
    T = sparse(zeros(length(V)));
    
    %fprintf('k=%d just before diving into WHILE\n',k);
    
    while k > 0
        %fprintf('I am in the yet another while loop with k=%d\n',k);
        T_BEST = sparse(zeros(length(V)));
        d_T_BEST = Inf;
        terminals_in_T_BEST = 0;
        vertices_of_T_BEST = [];
        
        %fprintf('will go through vertices now...\n');

        for s = 1:length(V)
            v = V(s);
            %fprintf('consider vertex %d - ',v);
            
            if D(r,v)~=Inf
                path = buildShortestPathfromAll_shortest_paths_BGL(r,v,P); 
                coords = [path(1:end-1)' path(2:end)'];
                T_r2v = sparse(zeros(length(V)));
                    for z = 1:length(path)-1
                        T_r2v(coords(z,1),coords(z,2)) = 1;
                    end
                    

                    %fprintf('will go through k_prime now (k=%d)...\n',k);
                    for k_prime = 1:k
                        T_prime = Ai(i-1,k_prime,v,X) + T_r2v;
%                        recursion_level = recursion_level + 1;
                        if isnan(T_prime)
                            T = NaN;
                            return
                        end
                        [rows columns] = find(T_prime ~= 0);
                        vertices_of_T_prime = union(rows,columns);
                        number_of_terminals_in_T_prime = length(intersect(vertices_of_T_prime,allTerminals));
                        %fprintf('number_of_terminals_in_T_prime = %d',number_of_terminals_in_T_prime);
                        
                        d_T_prime = (sum(sum(T_prime))+0)/number_of_terminals_in_T_prime;
                            if d_T_BEST > d_T_prime
                                T_BEST = T_prime;
                                d_T_BEST = d_T_prime;
                                terminals_in_T_BEST = number_of_terminals_in_T_prime;
                                vertices_of_T_BEST = vertices_of_T_prime;
                            end
                    end

            else
                %fprintf('bad!\n');

            end
        end

        T = T + T_BEST;                        
        k = k - terminals_in_T_BEST;
        X = setdiff(X,vertices_of_T_BEST);
        %pause
    end    
end
[rows, columns] = find(approximationTree ~= 0);
vertices_of_approximationTree = union(rows,columns);
SteinerNodes = setdiff(vertices_of_approximationTree,Terminals);
end