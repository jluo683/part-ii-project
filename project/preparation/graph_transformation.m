function [ adjacencymatrix nodeCosts ] = graph_transformation( points, points_type,he,w,r,R )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
alpha_M = 1;
alpha_C =1;
epsilon = 10^(-36);
global c;
global h;
c = length(find(strcmp(points_type,'C')));
m = length(find(strcmp(points_type,'M')));
h = length(find(strcmp(points_type,'H')));

Beta_C =zeros(1,c+m+2*h)+1;

n = (c+h)*(c+h-1)/2;

nodeCosts = sparse(zeros(1,n+c+m+2*h));
%update the adjacencymatrix for MANET node and MANET node and update the nodecost for corresponding MANET node
adjacencymatrix = sparse(zeros(c+m+2*h+n, c+m+2*h+n));
x = points(:,1);
y = points(:,2);
distance_between_MANET = sparse(zeros( m+h, m+h));
for i = c+1:c+m+h
    distance_between_MANET(i-c,1:m+h) = sqrt((x(i)-x(c+1:c+m+h)).^2+(y(i)-y(c+1:c+m+h)).^2);
end
[row,col] = find(distance_between_MANET<R);
if ~isempty(row) 
for j = 1:length(row)
    if row(j) ~= col(j)
        adjacencymatrix(row(j)+c,col(j)+c)=epsilon;
        adjacencymatrix(col(j)+c,row(j)+c)=epsilon;      
             %R^(alpha)
    end
      nodeCosts(c+row(j)) = R^alpha_M ;
      nodeCosts(c+col(j)) = R^alpha_M;
end
end
%update the adjacencymatrix for Cellular node and Cellular node
setU = [(1:c) (c+m+h+1:c+m+2*h)];
[cell_IDX cell_Center_X cell_Center_Y]  = whichHoneyCombCell(points,he,w,r) ;
dummynode = c+m+2*h+1;

for k = setU
    for j = setU(2:end)
        
            adjacencymatrix(k,dummynode) = epsilon;
            adjacencymatrix(dummynode,k) = epsilon;
            adjacencymatrix(j,dummynode) = epsilon;
            adjacencymatrix(dummynode,j) = epsilon;
            nodeCosts(dummynode) = Beta_C(getCindex(k))*(sqrt((x(getCindex(k))-cell_Center_X(getCindex(k)))^2+(y(getCindex(k))-cell_Center_Y(getCindex(k)))^2))^alpha_C + ...
                                    Beta_C(getCindex(k))*(sqrt((x(getCindex(j))-cell_Center_X(getCindex(j)))^2+(y(getCindex(j))-cell_Center_Y(getCindex(j)))^2))^alpha_C;
            dummynode = dummynode+1;
    end
end
% connect twisted hybridnode of MANET node and Cellular node
for k = 1 : h
    adjacencymatrix(c+m+k,c+m+k+h) =epsilon;
    adjacencymatrix(c+m+k+h,c+m+k) =epsilon;
end

function ind = getCindex(s)
ind = s; 
if s > c
     ind = s-h;     
 end
end

end
