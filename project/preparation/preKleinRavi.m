function [SteinerTree SteinerTreeCost SteinerTreeNodes time_taken_without_allShortestPaths] = preKleinRavi(A, C, Terminals)

epsilon = 10^(-36);

[n m] = size(A);
f = find(A-A', 1);

if n == m && isempty(f) && isempty(find(A<0, 1)) 
    
    V = 1:n;

else
    
    error('Check matrix A - it should be square and symmetric containing non-negative elements!');

end

if length(C)==n && isempty(find(C<0, 1)) 
    
    fprintf('matrices A and C seem to be OK... continuing\n');
    
else
    
    error('Check C (node costs) - it should be of the length %d and contain non-negative elements!',n);
    
end

LT = length(Terminals);

if LT < 2
    error('There should be at least 2 terminals!');
end

global D;
global P;
global edgeWeights;
global nodeCosts

nodeCosts = C;

% edgeWeights contains edge costs which include the original graph's edge
% costs and dispersed costs [c(u)+c(v)]/2

edgeWeights = A;

f = find(A==0); % zeros correspond to absent edges
edgeWeights(f) = NaN; % change zero elements to NaN's

% finding all pairs shortest paths of the modified edge weighted graph 
halfNodePairCosts = (repmat(nodeCosts,n,1) + repmat(nodeCosts,n,1)')/2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

edgeWeights = sparse(edgeWeights + halfNodePairCosts); 
z = find(edgeWeights == 0); % find zero elements which now correspond to existing edges
edgeWeights(z) = epsilon; % make such edges present
edgeWeights(f) = 0; % restore the absent edges (flip Nan's back to zeros)

% ENDOF creating edgeWeights of the weight-transformed graph


%D = zeros(n,n); P = D; % P is of same size as D

[D P] = all_shortest_paths(edgeWeights,struct('algname','floyd_warshall'));

%D = D - halfNodePairCosts; % D will contain path costs minus end node costs!
% ENDOF adjusting for end node half costs

start_time = tic;

BEST_COST = Inf;
BEST_v = NaN;

    for k = 1:n
        v = V(k);
        incrementalAdjacency = sparse(zeros(length(nodeCosts)));
        fprintf('Consider vertex %d\n',v);
        for s = 1:length(Terminals)
            t = Terminals(s); % consider terminal t
            fprintf('path to terminal %d\n',t);
            this_paths_upper_triangular_Adjacency = connect2node(v, t);
            incrementalAdjacency = incrementalAdjacency + this_paths_upper_triangular_Adjacency;            
        end
        
        f = find(incrementalAdjacency);
        incrementalAdjacency(f) = 1;
        [row col] = ind2sub(size(incrementalAdjacency), f);
        AllTreeNodes = unique(union(row,col));
        %degree_one_verticesIND = find(sum(incrementalAdjacency,2)==1);
        this_tree_cost = sum(sum(full(incrementalAdjacency.*A)))/2 + sum(nodeCosts(AllTreeNodes));
        
        if this_tree_cost < BEST_COST
           BEST_COST =  this_tree_cost;
           BEST_v = v;
           BEST_TREE = incrementalAdjacency;
           SteinerTreeNodes = setdiff(AllTreeNodes, Terminals);
           fprintf('This vertex best so far... cost = %f\n',BEST_COST);
        end
    end
    
    
time_taken_without_allShortestPaths = toc(start_time);
    
SteinerTree = BEST_TREE;

SteinerTreeCost = BEST_COST;




    function path_Adjacency = connect2node(v, node) 
        path2add = buildShortestPathfromAll_shortest_paths_BGL(v,node,P);
        
        L = length(path2add);
        path_Adjacency  = sparse(zeros(length(nodeCosts)));
        
        for q = 1:(L-1)            
            path_Adjacency(path2add(q),path2add(q+1)) = 1;            
            path_Adjacency(path2add(q+1),path2add(q)) = 1;            
        end
    end


end

