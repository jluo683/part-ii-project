function [ average s m highest lowest ] = data_display( output_table,l,b )
% this function is used to find out the mean and deviation for grouped data
% output_table is the data will be dealt with and l refers to the specific
% column of the table which will be the set of data are going to find out
% mean and standard deviation. for i = 1 
lambda  = unique(output_table(:,1));
f = find(output_table (:,1) == lambda(2) & output_table(:,8) == 1 & ~isnan(output_table(:,13)));% for a fixed lambda(lambda(1)), let the tuning parameter be 1 and find out the Charikar SteinerTree 
                                                                                         % is not nan which means we use the value from Charikar algorithm under a timeshreshold.
[values, order] = sort(output_table(f,7));
updated_table = output_table(f,:);
%addition_table = output_table(f-1,:);%used for when i = 2 and still use the value of klein-ravi for i = 1
%addition_sorted_table = addition_table(order,:);
sorted_table = updated_table(order,:); %group data in terms of proportion of terminals (percentage)
prop = unique(sorted_table(:,7));
props = [prop(find(unique(prop)<=10)')' 25 35 50 75 90];

j = 0;%used to find the number of data has the same proportion of terminals
k = 1;%used to index mean and standard deviation
m = length(output_table(:,7)) - length(f);%used to find how many times when Charikar's takes more than the threshold.
a = 1;%used to index the start entry for a specified proportion of terminals 
average = zeros(1,0);
s = zeros(1,0);
highest = zeros(1,0);
lowest = zeros(1,0);
n = 1;
for i = 1:length(props)
    if props(i)<=10
    j = length(find(sorted_table(:,7)==props(i)));
    else 
        j = length(find((sorted_table(:,7)< props(i)+5) & (sorted_table(:,7)>= props(i)-5)));
         %j = length(find(sorted_table(:,7)==props(i)));
    end
   
    highest(k) = max((sorted_table(a:a+j-1,l)-sorted_table(a:a+j-1,b)));
    lowest(k) = min((sorted_table(a:a+j-1,l)-sorted_table(a:a+j-1,b)));
    average (k) = mean(sorted_table(a:a+j-1,l)-sorted_table(a:a+j-1,b));
    s(k) = std(sorted_table(a:a+j-1,l)-sorted_table(a:a+j-1,b));
    for c= 1: j
    d(n:n+4,1) = props(i);
    d(n,2) = lowest(k);
    d(n+1, 2) = average(k) - s(k);
    d(n+2, 2) = average(k);
    d(n+3,2) = average(k) + s(k);
    d(n+4,2) = highest(k);
    n = n+5;
    end
    
    a = a+j;
    k = k+1;  
   

end
boxplot(d(:,2),d(:,1));
end