function [ average m ] = box_plot( output_table,l,b,i,d )
% this function is used to produce the plot of the SteinerTree cost or running time against the 
% proportion of terminals for grouped data and show the mean of the data 
% output_table is the data will be dealt with and l and b refer to the specific
% columns of the table which will be used to find out the difference and
% the mean of differnce. c will specify the recursion level.
% 
lambda  = unique(output_table(:,1));
f = find(output_table (:,1) == lambda(d) & output_table(:,8) == i & ~isnan(output_table(:,13)));% for a fixed lambda(lambda(1)), let the tuning parameter be 1 and find out the Charikar SteinerTree 
                                                                                         % is not nan which means we use the value from Charikar algorithm under a timeshreshold.
[values, order] = sort(output_table(f,7));
updated_table = output_table(f,:);
if i == 2
addition_table = output_table(f-1,:);%used for when i = 2 and still use the value of klein-ravi for i = 1
addition_sorted_table = addition_table(order,:);
end
sorted_table = updated_table(order,:); %group data in terms of proportion of terminals (percentage)
prop = unique(sorted_table(:,7));
props = [prop(find(unique(prop)<=10)')' 25 35 50 75 90];

k = 1;%used to index mean 
m = length(output_table(:,7)) - length(f);%used to find how many times when Charikar's takes more than the threshold.
a = 1;%used to index the start entry for a specified proportion of terminals 
average = zeros(1,0);
sorted_table(sorted_table(:,7)==11,7) = 10;
for c = 1:length(props)
    if props(c)<=10
    j = length(find(sorted_table(:,7)==props(c)));%used to find the number of data has the same proportion of terminals
    else
        d = find((sorted_table(:,7)< props(c)+5) & (sorted_table(:,7)>= props(c)-5));
        j = length(d);
        sorted_table(d,7) = props(c);
         %j = length(find(sorted_table(:,7)==props(i)));
    end
%     if j > 0 & i == 1
%     average (k) = mean(sorted_table(a:a+j-1,l)-sorted_table(a:a+j-1,b));
%     a = a+j;
%     k = k+1;  
%     else if i == 2 & j > 0 
%     average (k) = mean(sorted_table(a:a+j-1,l)-addition_sorted_table(a:a+j-1,b)); 
%     a = a+j;
%     k = k+1;  
%     end

end
% if i==1
% boxplot(sorted_table(:,l)-sorted_table(:,b),sorted_table(:,7));
% else 
%     boxplot(sorted_table(:,l) - addition_sorted_table(:,b), sorted_table(:,7));
% end
% sorted_table(:,7);
   boxplot(sorted_table(:,l), sorted_table(:,7));
end