function cell_IDX = whichVoroniCell(points, cell_centers)
%
%function C = whichVoroniCell(points, cell_centers)
%Input arguments
%points- an (x*2) array of points' coordinates
%cell_centers- an(kx2) array of coordinates of the base stations

all_points = [points; cell_centers];

D = max(pdist(all_points));

circleCentre = mean(all_points);

phi = linspace(0,2*pi,17)'; phi = phi(1:end-1);

additional_cell_centers = 10*D*[cos(phi) sin(phi)];

original_cell_centers = cell_centers;
[how_many_cell_centers mdummy] = size(original_cell_centers);
cell_centers = [cell_centers; additional_cell_centers];

[V, C] = voronoin(cell_centers);
[n, m] = size(points);

cell_IDX = zeros(n,1);


    for q = 1:length(C)
        
        if isempty(find(V(C{q},:)==Inf))
            current_cell = V(C{q},:);
            
            IN = inpolygon(points(:,1),points(:,2),current_cell(:,1),current_cell(:,2));
            f = find(IN==1);
            cell_IDX(f) = q;
        end
        %    plot(cell_centers(j,1),cell_centers(j,2),'b.')
        
    end



voronoi(cell_centers(:,1), cell_centers(:,2))
hold all
plot (points(:,1), points(:,2), 'r.', cell_centers(:,1), cell_centers(:,2),'g.');
axis equal

plot(original_cell_centers(:,1),original_cell_centers(:,2),'y*');

for k = 1:how_many_cell_centers
    
    text(original_cell_centers(k,1),original_cell_centers(k,2),num2str(k));
end

for k = 1:n
    
    text(points(k,1)+0.01,points(k,2),num2str(cell_IDX(k)));
end


end


