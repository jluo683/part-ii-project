function [x y] = test_function(L,n,to_plot)

coord = L*rand(1,2*n);

x = coord(1:n);
y = coord(n+1:end);
if to_plot == 1
    plot(x,y,'r.-');
end

