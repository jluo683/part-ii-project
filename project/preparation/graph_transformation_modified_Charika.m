function [adjacencymatrix first_dummy_node_index_in_Charikar] = graph_transformation_modified_Charika( points, points_type,he,w,r,R )
%UNTITLED3 Summary of this function goes here
% modified graph_transformation with only adding one fictious one for MANET
% node
alpha_M = 4;
alpha_C = 3;
epsilon = 10^(-36);

c = length(find(strcmp(points_type,'C')));
m = length(find(strcmp(points_type,'M')));
h = length(find(strcmp(points_type,'H')));
x = points(:,1);
y = points(:,2);
n = m+h;
Beta_C =zeros(1,c+m+h)+1;
adjacencymatrix = sparse(zeros(c+m+h+n, c+m+h+n));
first_dummy_node_index_in_Charikar = c+m+h+1;

%update the adjacencymatrix for MANET node and MANET node and update the nodecost for corresponding MANET node
distance_between_MANET = sparse(zeros(m,m));
for i = c+1:c+m+h
    distance_between_MANET(i-c,1:m+h) = sqrt((x(i)-x(c+1:c+m+h)).^2+(y(i)-y(c+1:c+m+h)).^2);
end
[row,col] = find(distance_between_MANET<R);
dummynode = c+m+h+1;
if ~isempty(row) 
for j = 1:length(row)
    if row(j) ~= col(j)
        adjacencymatrix(row(j)+c,dummynode)=R^alpha_M ;
        adjacencymatrix(dummynode, col(j)+c) = epsilon;
        dummynode = dummynode + 1;
        %R^(alpha)
    end      
end
%update the adjacencymatrix for Cellular node and Cellular node
setU = [(1:c) (c+m+1:c+m+h)];
[cell_IDX cell_Center_X cell_Center_Y]  = whichHoneyCombCell(points,he,w,r) ;
for k = setU
    for j = setU
        if k<j
            adjacencymatrix(k,j) = Beta_C(k)*(sqrt((x(k)-cell_Center_X(k))^2+(y(k)-cell_Center_Y(k))^2))^alpha_C + ...
                                   Beta_C(k)*(sqrt((x(j)-cell_Center_X(j))^2+(y(j)-cell_Center_Y(j))^2))^alpha_C;
            adjacencymatrix(j,k) =  adjacencymatrix(k,j);
        end
    end
end
end



end