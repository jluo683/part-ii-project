function [points points_type] = generateDeployment(L, lambda, p, Radius, varargin)
%
% function generateDeployment(L, lambda, p, Radius, TO_PLOT)
%
% Generates a size n sample of a Poisson Point Process within a square
% [0,L]X[0,L] with parameter lambda and marks the points 
% using 'C' for cellular with probability p(1); 
% using 'M' for hybrid with probability p(2), and
% using 'H' for MANET with probability p(3).
% The elements of p should sum up to 1.
% R is the MANET broadcast radius
%
% EXAMPLE
%
% [points points_type] = generateDeployment(10, 0.6, [0.5 0.4 0.1], 1.5, true) 

if sum(p)~=1 && ~isempty(p<0)
    error('Check vector p!');
end


optargin = size(varargin,2);

TO_PLOT = 1; % by default to plot

if ~isempty(varargin)
    
    if varargin{1}
        TO_PLOT = 1;
    else 
        TO_PLOT = 0;
    end
end

N = random('poisson', L^2*lambda);  %The number of points to generate

points = rand(N,2);

points = L.*points;


points_type = cell(N,1);

MNS = mnrnd(N,p,1); % a sample from multinomial distribution with vector-parameter p

points_type(1:MNS(1))                    = repmat({'C'},MNS(1),1);
points_type(MNS(1)+1 : MNS(1) + MNS(2))      = repmat({'M'},MNS(2),1);
points_type(MNS(1)+MNS(2)+1 : end)        = repmat({'H'},MNS(3),1);


if TO_PLOT
    MANET_RADIO_RANGE = Radius;
    for k = 1:N
        switch points_type{k}
            case 'C'
                plot(points(k,1),points(k,2),'.r'); hold all;
            case 'H'
                plot(points(k,1),points(k,2),'.b'); hold all;   
                drawCircle(points(k,1), points(k,2), MANET_RADIO_RANGE,'blue');
            case 'M'
                plot(points(k,1),points(k,2),'.g'); hold all;
                drawCircle(points(k,1), points(k,2), MANET_RADIO_RANGE,'green');
        end
    
    end
    title('Cellular (red nodes), MANET (green nodes) and Hybrid (blue nodes)');
    axis equal; 
    xlim([0,L+1]); ylim([0,L+1]);
end





function drawCircle(x,y,R,colour)
t = 0:0.1:2*pi;
x = x + R*cos(t);
y = y + R*sin(t);
switch colour
    case 'green'
        plot(x,y,'--g'); hold all;
    case 'blue'
        plot(x,y,'--b'); hold all;
end
end

end