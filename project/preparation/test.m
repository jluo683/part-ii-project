function [ SteinerTreeCost approximationTreeCost_c ] = test

%real size of the deployment and steinernode
%[ SteinerTreeCost approximationTreeCost_c ] = test(10,0.3,[0.5 0.4 0.1],1.6,true, 30, 30, 5, 6, [ 3,6,25],2 )
Klein_Ravi_SteinerTreeCost = 0;
Klein_Ravi_time = 0;
Charikar_SteinerTreeCost = 0;
Charikar_time =0;

L = 10; % size of the deployment square(1)
for j = 1 : 50
lambda = 0.3; % intensity of the Poisson point process, will be a changing parameter, 1.426, for manet readius become 1, everything will be conneted
              % 0.3,04,0.6,0.9,1.2,1.43,1.6(7)
p = [0.5 0.4 0.1] % p = [pC, pM, pH] - a vetor of node type probabilities, hybrid node enhance connectivity pH = 0,0.1,0.25,0.3,0.5, pC/pM = 0.1, 0.25,0.5,0.75,0.9, pc+ pm + ph = 1, pc = (1 - ph)/(1+ pc/Pm)(25)
he = L+2; % height of the square for the triangular grid of base stations
w = L+2; % width of the square for the triangular grid of base stations
r = 1; % radius of the hexagonal cell, 0.5, 1, 2,3,4(5)
MANET_R = 1.6; % MANET broadcast communication radius, 0.5 ,1 ,2 (3)
number_of_terminals = 3;% propotional to maximum_element, 2 to min (10, max_element) + 10%, 25%, 50% of max_element  
i = 1; % Charikar's performance/accuary tuning parameter, 1 to floor log2k+1, where k = number of terminals - 1, spot for a given i, wait more than 200 minutes, then skip theh rest, finnally integarate i.

%lambda, p, Radius, varargin, he, w, r, R,Terminals,i 

[points points_type] = generateDeployment(L, lambda, p, MANET_R);

[nNodes dummy] = size(points);

%[ adjacencymatrix_k nodeCosts_k ] = graph_transformation( points, points_type,he,w,r,MANET_R);
[adjacencymatrix_k nodeCosts_k] = graph_transformation_without_dummy_nodes( points, points_type,he,w,r,MANET_R);


[ci sizes] = components(adjacencymatrix_k);
[max_elements IND] = max(sizes);
f = find(ci(1:nNodes) == IND );

sample_of_random_indices = 0;

while length(unique(sample_of_random_indices))~=number_of_terminals

    sample_of_random_indices = randi(length(f),1,number_of_terminals);

end

Terminals = f(sample_of_random_indices);


adjacencymatrix_c  = graph_transformation_modified_Charika( points, points_type,he,w,r,MANET_R);

fprintf('both graph transformation completed\n');

tic
[SteinerTree, SteinerNodes, Klein_Ravi_SteinertreeCost] = Klein_Ravi(adjacencymatrix_k, nodeCosts_k, Terminals);
Klein_Ravi_SteinerTreeCost = [Klein_Ravi_SteinerTreeCost' Klein_Ravi_SteinertreeCost' ]';
Klein_Ravi_Time = toc;
Klein_Ravi_time = [Klein_Ravi_time' Klein_Ravi_Time']';
%fprintf('Time taken to perform Klein-Ravi is %f seconds\n',Klein_Ravi_time);

tic
[approximationTree_c Charikar_SteinertreeCost] = dirSteinerTree(adjacencymatrix_c, setdiff(Terminals,Terminals(1)), Terminals(1), i);
Charikar_SteinerTreeCost = [Charikar_SteinerTreeCost' Charikar_SteinertreeCost']'
Charikar_Time = toc;
Charikar_time = [Charikar_time' Charikar_Time']';
%fprintf('Time taken to perform Charikar et als is %f seconds\n',Charikar_time);
end
Test_result = table(Klein_Ravi_SteinerTreeCost, Charikar_SteinerTreeCost,Klein_Ravi_time,Charikar_time);
writetable(Test_result,'/Users/luo/repos/project/preparation/Test_result.csv');
end

