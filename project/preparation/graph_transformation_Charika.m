function adjacencymatrix = graph_transformation_Charika( points, points_type,he,w,r,R )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
alpha_M = 1;
alpha_C =1;
epsilon = 10^(-36);

c = length(find(strcmp(points_type,'C')));
m = length(find(strcmp(points_type,'M')));
h = length(find(strcmp(points_type,'H')));
x = points(:,1);
y = points(:,2);
n = (m+h)*2;
Beta_C =zeros(1,c+m+h)+1;
adjacencymatrix = sparse(zeros(c+m+h+n, c+m+h+n));
%update the adjacencymatrix for MANET node and MANET node and update the nodecost for corresponding MANET node
distance_between_MANET = sparse(zeros(m,m));
for i = c+1:c+m+h
    distance_between_MANET(i-c,1:m+h) = sqrt((x(i)-x(c+1:c+m+h)).^2+(y(i)-y(c+1:c+m+h)).^2)
end
[row,col] = find(distance_between_MANET<R);
dummynode = c+m+h+1
if ~isempty(row) 
for j = 1:length(row)
    if row(j) ~= col(j)
        adjacencymatrix(row(j)+c,dummynode)=  epsilon;
        adjacencymatrix(dummynode,dummynode + 1)= R^alpha_M;
        adjacencymatrix(dummynode+1, col(j)+c) =  epsilon;
        dummynode = dummynode + 2;
        %R^(alpha)
    end      
end
%update the adjacencymatrix for Cellular node and Cellular node
setU = [(1:c) (c+m+1:c+m+h)];
[cell_IDX cell_Center_X cell_Center_Y]  = whichHoneyCombCell(points,he,w,r) ;
for k = setU
    for j = setU(2:end)
        
            adjacencymatrix(k,j) = Beta_C(k)*(sqrt((x(k)-cell_Center_X(k))^2+(y(k)-cell_Center_Y(k))^2))^alpha_C + ...
                                    Beta_C(k)*(sqrt((x(j)-cell_Center_X(j))^2+(y(j)-cell_Center_Y(j))^2))^alpha_C;
            adjacencymatrix(j,k) =  adjacencymatrix(k,j);
    end
end
end


end

