function [SteinerTree, SteinerNodes] = KleinRavi(A, nodeCosts, terminals)
% A is the adjacency matrix of the graph, nodeCosts are node costs, and
%terminals is the set of terminal nodes to be connected. The output 
%arguments are SteinerTree - the adjacency matrix of the Steiner tree
%and SteinerNodes - the nodes of the Steiner tree minus the set of terminals.
%   Detailed explanation goes here
g = graph (A);
[n m] = size(A);
for t = terminals
    B = zeros(n,m);
    B(t.x,t.y)= t.cost;
   set = union (set,struct('Node',t,'Adj',B));
end
while size(set)>1
    for v = g.nNodes
        for i = 0:size(set)-1
        [minimal_distance[v i], closest_node[v i]]  = find_minimal_distance(g,v,set[i]);
        end
        quicksort(minimal_distance);%should it be set or minimal_distance?
        for k =2:size(set)
           for j = 1: k
            f[v k] = minimal_distance(v, j) + f(v, k);
           end
         q[v k] = (v.cost+f[v k])/k;
        end
    end
    [v k] = find( q == min(q));
    for i = 1: k
    M = merge(M,set[i],v);
    set.(i) =[] ;
    end
    set = union( set, M);   
end

SteinerTree = M;
SteinerNodes = setdiff(SteinerTree, terminals);%not quite sure whether it is about the array difference but i think it's not


end

