classdef graph < handle
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(Dependent = true, SetAccess = private)
        nodeDegree
        edge
    end
    properties
        Adj
        nodeWeights
        nNodes
        D
        P
        epsilon
    end
    methods
        function obj = graph(A,varargin)
            obj.Adj = sparse(A);
            optargin = size (varargin, 2);%no.of optional arguments
            [n m] = size(A);
            obj.nNodes = n;
            obj.D = NaN;
            obj.P = NaN;
            obj.epsilon = 10^(-10)
            if optargin == 0
                obj.nodeWeights = zeros(1,obj.nNodes);
            end
            if optargin == 1 %edgeweight has been passed
                if length(varargin{1}) == obj.nNodes
                    obj.nodeWeights = varargin{1};
                else
                    error('check the size of node cost input!\n');
                end
            end
            fprintf('an instance of graph class has been created\n');
        end
        
        function obj = kill(obj)
            delete(obj);
        end
        
        function A = modified_Adj(obj,A)
            [n m] = size(A);
            obj.nNodes = n;
            for i = 1 :n
                for j= 1:m
                    A(i,j) = A(i,j) + (obj.nodeWeights(i)+obj.nodeWeights(j))/2;
                end
            end
        end
        
        function [minimal_distance, closest_node]  = find_minimal_distance(obj,x,set_of_nodes)
            if ~isempty(setdiff(set_of_nodes,1:obj.nNodes))
                error('check the set of nodes!');
            else
                if isnan(obj.D)
                    [obj.D, obj.P] = obj.calculateDP();
                    
                end
                
                [minimal_distance, IND] = min(obj.D(x,set_of_nodes));
                closest_node = set_of_nodes(IND);
            end
        end
        
        
        function [ path cost pathAdjacency] = find_shortest_path(obj, x,y)
            
            if isnan(obj.D)
                fprintf('Calculating all pairs shortest paths\n');
                [obj.D, obj.P] = obj.calculateDP();
            end
            %fprintf('No need to calculate all pairs shortest paths\n');
            [n m] = size(obj.P);
            path = zeros(1,n);
            path(1) = y;
            last_node= y;
            k = 1;
            while(last_node ~= x)
                k = k +1;
                last_node =obj.P(x,last_node);
                path(k) = last_node;
            end
            path = fliplr( path(1:k));
            cost = obj.D(x, y);
            pathAdjacency = sparse(zeros(obj.nNodes));
            for k  = 1:length(path)-1
                pathAdjacency(path(k),path(k+1)) = obj.Adj(path(k),path(k+1));
                pathAdjacency(path(k+1),path(k)) = obj.Adj(path(k),path(k+1));
            end
        end
        
         function [path cost pathAdjacency] = find_directed_shortest_path(obj, x,y)
            
            if isnan(obj.D)
                [obj.D, obj.P] = obj.calculateDP();
            end
            [n m] = size(obj.P);
            path = zeros(1,n);
            path(1) = y;
            last_node= y;
            k = 1;
            while(last_node ~= x)
                k = k +1;
                last_node =obj.P(x,last_node);
                path(k) = last_node;
            end
            path = fliplr( path(1:k));
            cost = obj.D(x, y);
            pathAdjacency = sparse(zeros(obj.nNodes));
            for k  = 1:length(path)-1
                pathAdjacency(path(k),path(k+1)) = obj.Adj(path(k),path(k+1));
                pathAdjacency(path(k+1),path(k)) = obj.Adj(path(k+1),path(k));
            end
        end
        
        
        function quickPlot(obj, varargin)
            
            optargin = size(varargin,2);
            
            if optargin == 0
                xy = rand(obj.nNodes,2);
            else
                xy = varargin{1};
            end
            
            gplot(obj.Adj,xy);
            hold all
            for k =1:obj.nNodes
                text(xy(k,1),xy(k,2),num2str(k));
                plot(xy(k,1),xy(k,2),'g.');
            end
        end
        
    end
    
    methods(Access = protected)
        function [D_adjusted, P_adjusted] = calculateDP(obj)
            Amodified = obj.Adj;
            f = find(obj.Adj == 0);
            Amodified (f) = NaN;
            for k = 1 : obj.nNodes
                Amodified (k,:) = Amodified(k,:) + (obj.nodeWeights(k) + obj.nodeWeights)/2;
            end
            z = find (Amodified == 0);
            Amodified (z) = obj.epsilon;
            Amodified (f) = 0;
            [D_adjusted, P_adjusted] =   all_shortest_paths(Amodified,struct('algname','floyd_warshall'));
            for k = 1 : obj.nNodes
                D_adjusted (k,:) = D_adjusted(k,:) + (obj.nodeWeights(k) + obj.nodeWeights)/2;
            end
        end
    end
    
end

