function [SteinerTree SteinerNodes SteinerTreeCost time_taken_without_allShortestPaths] = Klein_Ravi(A, nodeCosts, terminals)
%
% [SteinerTree, SteinerNodes] = Klein_Ravi(A, nodeCosts, terminals)
% 
% A is the adjacency matrix of the graph, nodeCosts are node costs, and
%terminals is the set of terminal nodes to be connected. The output
%arguments are SteinerTree - the adjacency matrix of the Steiner tree
%and SteinerNodes - the nodes of the Steiner tree minus the set of terminals.
%
% EXAMPLE
%
% clear all; load graphs/minnesota.mat % this graph is from matlab-bgl library
% N = 55;
% A = A(1:N,1:N);
% f = find(A~=0); A(f) = 10^(-36);
% C =  unifrnd(1,20,1,N); % C contains node costs
% Terminals = [1, 8, 34, 2, 40, 36];
% [SteinerTree SteinerNodes SteinerTreeCost] = Klein_Ravi(A, C, Terminals);


g = graph(A);

start_time = tic;

n = g.nNodes;%n vertices in the original graph
LT = length(terminals);% this is how many terminals we have

Trees = cell(1,LT);%this is the array of trees(it is a cell array of elements which
%are trees-each of them represented as a structure

for k = 1:LT
    TreeAdj = sparse(zeros(n));% adjacency matrix in the sparse form-all zero elements
    % at initialisation
    t = terminals(k);% take the k-th terminal
    new_tree = struct('Vertices',t,'Adjacency',TreeAdj);%create a tree structure
    %with this terminal
    Trees{k} = new_tree; %record the newly created one-element tree in Trees
end

while LT > 1% there is more than one tree in Tree
    minimal_distance = sparse(zeros(n,LT));
    closest_node = sparse(zeros(n,LT));
    f = sparse(n,LT);
    q = sparse(n,LT);
    
    for i = 1:n
        
        for j = 1:LT% for each vertex calculate cost of connecting i and Tree{j}
            Trees{j}.Vertices;
            [minimal_distance(i,j), closest_node(i,j)]  = g.find_minimal_distance(i,Trees{j}.Vertices);
        end
        
        [val idx] = sort(minimal_distance(i,:));%sort the costs in the ascending order
        
        f(i,:) = cumsum(val);
        
        q(i,:) = (nodeCosts(i)+f(i,:))./(1:LT);
        
    end
    q(:,1) = Inf;
    q_full = full(q);
    
    [min_distance_nodes min_distance_trees] = find( q_full == min(min(q_full)));
    
    for l = 1: min_distance_trees(1)-1%merge trees
        T_merged = struct('Vertices',[],'Adjacency',sparse(zeros(n)));%preparing for the new tree: no nodes and zero adjacency matrix so far
                
        [path_l, cost_l, pathAdjacency_l] = g.find_shortest_path(min_distance_nodes(1),closest_node(min_distance_nodes(1),idx(l)));
        [path_l_plus_one, cost_l_plus_one, pathAdjacency_l_plus_one] = g.find_shortest_path(min_distance_nodes(1),closest_node(min_distance_nodes(1),idx(l+1)));
        
               
        T_merged.Vertices = union (min_distance_nodes(1), [Trees{idx(l)}.Vertices, Trees{idx(l+1)}.Vertices, path_l, path_l_plus_one]);
        new_tree_adjacency = Trees{idx(l)}.Adjacency + Trees{idx(l+1)}.Adjacency + pathAdjacency_l + pathAdjacency_l_plus_one;
        
        T_merged.Adjacency = new_tree_adjacency;% record the adjacency matrix of the merged trees//put it outside the loop.
        Trees{idx(l+1)} = T_merged;%update cell array
        Trees{idx(l)} = [];%delete the original tree
        
        LT = LT - 1;
        %decrease the size of the tree
    end
    
    Trees = Trees(~cellfun(@isempty, Trees));
    
end

SteinerTree = Trees{1};
idx = find(SteinerTree.Adjacency);%find non-zero elements in this matrix
SteinerTree.Adjacency(idx) = A(idx);%restore the true weights

SteinerTreeCost = sum(nodeCosts(SteinerTree.Vertices))+sum(SteinerTree.Adjacency(idx))/2;%add node cost& edgecost

SteinerNodes = setdiff(SteinerTree.Vertices,terminals);

time_taken_without_allShortestPaths = toc(start_time);

kill(g);

end