function pathRes = buildShortestPathfromAll_shortest_paths_BGL(u,v,P)
% only use this function to reconstruct shortest paths when you used BGL's
% all_shortest_paths function, e.g.: 
% [D P] = all_shortest_paths(A,struct('algname','floyd_warshall'));




[n m] = size(P);

pathRes = zeros(1,n); 

k = 0;

while v ~= 0    
    k = k+1;
    pathRes(k) = v;     
    v = P(u,v); 
end; 

if k>0
    pathRes = fliplr(pathRes(1:k));
else 
    pathRes = [];
end