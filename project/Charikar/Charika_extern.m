function [SteinerTree, SteinerNodes, SteinerTreeCost] = Charika_extern(A, r, X, i, k)
% A is the adjacency matrix of the directed graph, r is a given root,
% X is the set of terminal nodes to be reached from r, is the tuning parameter resposible for
% accuracy, k is the size of X.The output arguments are SteinerTree - the adjacency matrix of
% the Steiner tree, SteinerNodes - the nodes of the Steiner tree minus the set of terminals
% and SteinerTreeCost - the cost of connecting terminals and root together.
global g;
g = graph(A);
n = g.nNodes;%n vertices in the original graph
path11_dummy = g.find_shortest_path(1,1);%always recalculate D and P
i_initial = min(floor(log2(length(X)))+1,i);
[SteinerTree,SteinerTreeCost] = Charika_intern(r, X, i_initial, k);
[row,col] = find(SteinerTree);
SteinerNodes = setdiff(union(row,col),union(X,r));
idx = find(SteinerTree);
            [row,col] = find(SteinerTree);
            SteinerTree(idx) = g.Adj(idx);
           SteinerTreeCost = sum(sum (full(SteinerTree(idx))));


    function [SteinerTree,SteinerTreeCost] = Charika_intern(r, X, i, k)
       f = find(g.D(r,X)~=Inf & g.D(r,X)~=0);
        if length(f)< k
           SteinerTree = sparse(zeros(n));
            SteinerTreeCost = NaN;
             fprintf('returning empty tree\n');
            return
        end
         fprintf('i=%d\n',i);

        if i == 1
                   
            [val idx] = sort( g.D(r,X));
            T_merged = sparse(zeros(n));
            [path_l, cost_l, T_merged] = g.find_directed_shortest_path(r,X(idx(1)));
            
            for l = 2: k %merge trees
                
                [path_l, cost_l, pathAdjacency_l] = g.find_directed_shortest_path(r,X(idx(l)));
                T_merged = T_merged + pathAdjacency_l;
          
            end
            
            SteinerTree = T_merged;
            idx = find(SteinerTree~=0);
            SteinerTree(idx)= g.Adj(idx);   
            SteinerTreeCost = sum(sum (full(SteinerTree(idx))));

            return
        end
        SteinerTree = sparse(zeros(n));
        while k > 0
            T_BEST = sparse(zeros(n));
            d_T_BEST = Inf;% the cost of T_Best
            for j = 1 : n
                if g.D(r,j) ~= Inf
                    [ path, cost, pathAdjacency] = g.find_directed_shortest_path(r,j);
                    for k_prime = 1:k
                        T_prime = sparse(zeros(n));

                        [SteinerTreeInternal,SteinerTreeInternalCost] = Charika_intern( j, X, i-1, k_prime);
                        T_prime = pathAdjacency + SteinerTreeInternal
                        d_T_prime = sum(sum (full(T_prime(find(T_prime)))))
                        if d_T_BEST > d_T_prime
                            T_BEST = T_prime;
                            d_T_BEST = d_T_prime;
                        end
                    end
                end
            end
          
          
            SteinerTree = SteinerTree + T_BEST;
            
            
            [row,col] = find(T_BEST);
            T_BEST_Vertices = union(row,col);
            k = k - length( intersect(X,T_BEST_Vertices));
            X = setdiff(X,T_BEST_Vertices);
        %pause
        end
        
        
        
        %       SteinerTree.Vertice = union(SteinerTree.Vertice,T_BEST.Vertice);
        %       SteinerTree.Adajcency = SteinerTree.Adjacency + T_BEST.Adjacency;
        %       idd = find(SteinerTree.adjacency);
        %       SteinerTree.Adjacency(idd) = A_in(idd);
        %       C = intersect(X,T_BEST.Vertice);
        %       k = k - size(C);
        %       SteinerNodes = setdiff(SteinerTree.Vertices,X);
        %       SteinerTreeCost = D_T_BEST;
    end


end

